//
//  PCSockets.m
//  SocketApp
//
//  Created by Elvis on 8/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "PCSockets.h"

#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif

//extern static NSString *m_strStatus;

@implementation PCSockets



static NSString *m_strStatus [] = { 
	@"kCFStreamStatusNotOpen", 
	@"kCFStreamStatusOpening",
	@"kCFStreamStatusOpen",
	@"kCFStreamStatusReading",
	@"kCFStreamStatusWriting",
	@"kCFStreamStatusAtEnd",
	@"kCFStreamStatusClosed",	
	@"kCFStreamStatusError" 
};



/*
- (id) delegate {
	
	return delegate;
	
	//return NULL; // delegate;
}	
*/

//@synthesize PCSockets;

- (void)setIPAddress:(NSString *)sIPAddress {
	
	m_host = (CFStringRef)sIPAddress;
	m_sIPAddress = sIPAddress;
}


- (void)setPort:(UInt32)port {
	
	m_port = port;
}	



- (void)socketConnect {
	NSLog(@" PCSockets::socketConnect()");
	NSLog(@"   PCSockets::m_sIPAddress = %@",m_sIPAddress);
	//CFStringRef host = (CFStringRef)m_sIPAddress;
	
	//m_port = 11000;
	
	if (m_bConnecting)
		return;
	
	m_bConnecting = TRUE;
		
	if ((m_host == NULL) || (m_port == 0))
		return;
	
	CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, m_host, m_port, &m_readStream, &m_writeStream);
	
	CFWriteStreamSetProperty(m_writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);

	CFReadStreamSetProperty(m_readStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
	
	m_bConnecting = FALSE;	
	
}


- (void)socketOpen {
	NSLog(@" PCSockets::socketOpen()");
	
	if (m_writeStream == NULL)
		return;
	
	if (m_bOpenning)
		return;
	
	m_bOpenning = TRUE;
	
	Boolean b = CFWriteStreamOpen(m_writeStream);	
	
	m_bOpenning = FALSE;

	NSLog(@" CFWriteStreamOpen() = %@", b ? @"TRUE" : @"FALSE");
	
	
	Boolean br = CFReadStreamOpen(m_readStream);
	NSLog(@" >>>>>>>>>>>>>>>>>>>  CFReadStreamOpen() = %@", br ? @"TRUE" : @"FALSE");

}	


- (void)connectAndOpen {
	NSLog(@"PCSockets::connectAndOpen()");
	
	if (m_bConnectingAndOpenning)
		return;
	
	m_bConnectingAndOpenning = TRUE;

	[self socketConnect];
	[self socketOpen];

	m_bConnectingAndOpenning = FALSE;
}

- (Boolean)socketReceive2 {
	return  TRUE;
}

- (Boolean)okToSend {

	if (m_writeStream == NULL) 
		return FALSE;
	
	CFStreamStatus outputStatus = CFWriteStreamGetStatus(m_writeStream);
	
	//if ((outputStatus = kCFStreamStatusOpening) || (outputStatus == kCFStreamStatusOpen))
	if (outputStatus == kCFStreamStatusOpen)
		return TRUE;
	else 
		return FALSE;
	
}	
	
	
- (int)socketSend:(NSString *)s {
	NSLog(@"PCSockets::socketSend msg = %@",s);
	
	if (m_writeStream == NULL) 
		return -1;

	
	CFStreamStatus outputStatus = CFWriteStreamGetStatus(m_writeStream);	
	NSLog(@"socketSend() status = %@", m_strStatus[outputStatus]);	
	
	if (outputStatus != kCFStreamStatusOpen)
	{	
		NSLog(@"  CFStreamStatus != kCFStreamStatusOpen");
		return -1;
	}		
	
//	const UInt8 sCheckStatus[] = "PCSockets SEND!!!!!";
	//CFWriteStreamWrite(m_writeStream,sCheckStatus,strlen((char*)sCheckStatus));

	
	UInt8 buf2[256];
	memcpy(buf2, [s UTF8String], [s length]+1);
	
	// res: -1 = errorr, 0 = filled to capacity, > 0 number of bytes written
	CFIndex res = CFWriteStreamWrite(m_writeStream,buf2,strlen((char *)buf2));

	NSLog(@"socketSend res = %d",res);
	
	return (int)res;

}




- (int)socketReceive1 {
	NSLog(@"PCSockets::socketReceive()");
	
	if (m_readStream == NULL) 
		return -1;
	
	
	CFStreamStatus streamStatus = CFReadStreamGetStatus(m_readStream);	
	NSLog(@"socketReceive() status = %@", m_strStatus[streamStatus]);	
	
	if (streamStatus != kCFStreamStatusOpen)
	{	
		NSLog(@"  CFStreamStatus != kCFStreamStatusOpen");
		return -1;
	}	
	
	
	Boolean b = CFReadStreamHasBytesAvailable(m_readStream);
	
NSLog(@" CFReadStreamHasBytesAvailable(m_readStream) = %@", b ? @"TRUE" : @"FALSE");
	
	if (!b)
		return -1;
	
//	memcpy(buf2, [s UTF8String], [s length]+1);
// res: -1 = errorr, 0 = filled to capacity, > 0 number of bytes written
	
	//CFIndex bufferLength
	CFIndex bufferLength = 256;
	UInt8 buf2[bufferLength];
	CFIndex len = CFReadStreamRead(m_readStream,buf2,bufferLength);	
	
	
//NSString *s7 =  @"test";
	
	
	if (len > 0)
	{		
		
//		NSString *path = [[NSString alloc]   initWithString:@"...."];		
		
//		NSString *s3 = [NSString initWithString:s7];		
		
//NSString *s = [NSString stringWithUTF8String: (const char*)buf2];		

//- (id)initWithBytes:(const void *)bytes length:(NSUInteger)len encoding:(NSStringEncoding)encoding;	
//NSString *s2 = [[NSString alloc] initWithBytes:(const void *)buf2 length:(NSUInteger)len encoding:(NSStringEncoding)NSASCIIStringEncoding];			

//NSString *s1 = [NSString initWithBytes:(const void *)buf2 length:(NSUInteger)len encoding:(NSStringEncoding)NSASCIIStringEncoding];		

//NSString *theString = [NSString stringWithCString:(const char *)buf2 length:len];	
		
/*		
		NSData *data = [[NSMutableData alloc] init];		
	//- (void)appendBytes:(const void *)bytes length:(NSUInteger)length;	
		[data appendBytes:(const void *)&buf2 length:(NSUInteger)bufferLength];
	
	NSString *serverText = [[NSString alloc]
							initWithData:data
							encoding:NSASCIIStringEncoding];
*/	


		NSString *s2 = [[NSString alloc] initWithBytes:(const void *)buf2 length:(NSUInteger)len encoding:(NSStringEncoding)NSASCIIStringEncoding];					
		
		NSLog(@"\n\n >>>>>>>>>> CFReadStreamRead(...) = %@\n\n",s2);	
	
		[s2 dealloc];	
		
	}	
	
	return (int)len;
	
//	return 0;
}







- (int)status {

	if (m_writeStream == NULL)
		return -1;
	
	NSLog(@" STATUS NSError %@", (NSError *)CFWriteStreamCopyError(m_writeStream));	
	
	return (int)CFWriteStreamGetStatus(m_writeStream);
	
}	


- (Boolean) boolStatus {

	//socketSend: -1 = errorr, 0 = filled to capacity, > 0 number of bytes written
	
	// 0 = filled to capacity -> SHOULD THIS BE STATUS TRUE ?
	
	return m_bStatus = [self socketSend:@"status check"] > 0 ? TRUE : FALSE;
	
/*	
	if ([self socketSend:@"status check"] <= 0) 
		return m_bStatus = FALSE;
*/	
	
	
//	return m_bStatus;
}	



- (Boolean)status2 {
	NSLog(@" Boolean PCSockets::status()");

	//return FALSE;
	
	if (m_writeStream == NULL)
		return FALSE;
	
	CFStreamStatus outputStatus = CFWriteStreamGetStatus(m_writeStream);
	
	if ( (outputStatus == kCFStreamStatusOpen) || (outputStatus == kCFStreamStatusOpening) )
		return TRUE;
	else 
		return FALSE;

	
}	



- (void)socketClose {
	

	if (m_writeStream == NULL)
		return;
	
	CFWriteStreamClose(m_writeStream);


	if (m_readStream == NULL)
		return;

	CFReadStreamClose(m_readStream);	
	
}	



- (void)setString:(NSString *)s {
	NSLog(@"setString = %@",s);
	m_sTest = s;
	
}

- (NSString *)getString {
	NSLog(@"getString");	
	return m_sTest;
	
}


- (void)test1 {
	NSLog(@"test1()");
}

- (Boolean) getPrivate {
	return m_bPrivate;
}	



#pragma mark - dispatch queue timer

- (void)updateStatus {
	
	NSLog(@"  updateStatus");

	if (m_writeStream == NULL)
		return;
	
	CFStreamStatus outputStatus = CFWriteStreamGetStatus(m_writeStream);
	
	//if ( (outputStatus == kCFStreamStatusOpen) || (outputStatus == kCFStreamStatusOpening) )
	if (outputStatus == kCFStreamStatusOpen)
	{	
		NSLog(@"  updateStatus : outputStatus == kCFStreamStatusOpen");
		m_bStatus = TRUE;
	}	
	else if (outputStatus == kCFStreamStatusOpening)
	{	
		NSLog(@"  updateStatus : outputStatus == kCFStreamStatusOpening");
		m_bStatus = FALSE;
	}
	else 
	{
		NSLog(@"  updateStatus : else [self connectAndOpen]");
		[self connectAndOpen];
		m_bStatus = FALSE;
	}

} // updateStatus



- (void)log_PCSockets {

	NSLog(@"log_PCSockets");
	
}	


- (void)initDispatchQueueTimer {

	m_Queue = dispatch_queue_create("X10 Controller Dispatch Queue Label", NULL);	
	m_timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, m_Queue);
	
	dispatch_source_set_timer(m_timerSource, DISPATCH_TIME_NOW, NSEC_PER_SEC * 2.0, 0); // 2 seconds	
		
	dispatch_source_set_event_handler(m_timerSource, ^{ 
		dispatch_async(dispatch_get_main_queue(), ^{ 
			[self updateStatus]; }); });
		
	dispatch_resume(m_timerSource);		
	
}	


#pragma mark - NSObject


- (id)init {
	
	NSLog(@"-(id)init - PCSockets.m/h\n\n");
	
	m_host = NULL;
	m_port = 0;

	m_writeStream = NULL;
	m_readStream  = NULL;
	
	m_bStatus = FALSE;
	
	m_bConnectingAndOpenning = FALSE;
	m_bConnecting = FALSE;
	m_bOpenning   = FALSE;
	
	m_bPrivate = TRUE;
	m_bPublic  = TRUE;
	
	[self initDispatchQueueTimer];

	return [super init];
	
}	


- (void)dealloc {
	
	NSLog(@"dealloc - before super");
    
	[super dealloc];
	
	NSLog(@"dealloc - after super");
}


@end
