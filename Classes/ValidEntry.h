//
//  ValidEntry.h
//  X10Controller
//
//  Created by Elvis on 8/23/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ValidEntry : NSObject {

}


- (void)isValidEntry3;

- (void)isValidEntry2:(UITextField *)textField;

- (BOOL)isValidEntry:(UITextField *)textField;

- (BOOL)isValidEntry3:(NSString *)sTextField;

- (BOOL)isValidEntry4:(NSString *)sTextField val:(NSString *)sValue;



@end
