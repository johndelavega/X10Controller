//
//  PersistentInfo.h
//  X10Controller
//
//  Created by Elvis on 5/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PersistentInfo : NSObject {

}



- (void)saveToUserDefaults:(NSString *)sValue : (NSString *)sKey;

- (NSString *)retrieveFromUserDefaults:(NSString *)sKey;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave;

- (void)saveToUserDefaultsBool:(BOOL)bValue : (NSString *)sKey;
- (BOOL)retrieveFromUserDefaultsBool:(NSString *)sKey;




- (void)test1a;


@end
