//
//  MainViewController.h
//  X10Controller
//
//  Created by John on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FlipsideViewController.h"

#pragma message ("test1 egd2")

@class PCSockets;
@class PersistentInfo;


@interface MainViewController : UIViewController <FlipsideViewControllerDelegate> {

	IBOutlet UILabel *lblBuild;

	IBOutlet UILabel *lblDeviceName1;
	IBOutlet UILabel *lblDeviceName2;
	IBOutlet UILabel *lblDeviceName3;
	IBOutlet UILabel *lblDeviceName4;
	IBOutlet UILabel *lblDeviceName5;
	
	
	
	PCSockets *m_pcs;
	
	PersistentInfo *m_persistInfo;

	IBOutlet UITextView  *textViewDebug; // debug
	IBOutlet UITextField *textTest;	
	
	NSString *m_sIPAddress;
	NSString *m_sPortNumber;

	NSString *m_sIPAddressOld;
	NSString *m_sPortNumberOld;
	
	
	IBOutlet UILabel *lblStatus;

	IBOutlet UILabel *lblConnected;
	IBOutlet UILabel *lblConnectIPandPort;
	IBOutlet UIActivityIndicatorView *activityIndicator;
	

	dispatch_queue_t  m_myQueue; // = dispatch_queue_create("X10 Controller Dispatch Queue Label MainViewController", NULL);	
	dispatch_source_t m_timerSource; // = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, myQueue);
	BOOL              m_bTimerSourceResumed; 	
	
	BOOL m_bIsFirstRunInitialized;
	
	bool m_bIOSVersionSent;
	
	Boolean m_bLog; // similar variable located in NSLOG1.h

@private
	int m_test;
	
}

@property (nonatomic, retain) UILabel *lblConnected;
@property (nonatomic, retain) UILabel *lblConnectIPandPort;
@property (nonatomic, retain) UIActivityIndicatorView *activityIndicator;


@property (nonatomic, retain) UILabel *lblStatus;

@property (nonatomic, retain) UILabel *lblBuild;

@property (nonatomic, retain) UILabel *lblDeviceName1;
@property (nonatomic, retain) UILabel *lblDeviceName2;
@property (nonatomic, retain) UILabel *lblDeviceName3;
@property (nonatomic, retain) UILabel *lblDeviceName4;
@property (nonatomic, retain) UILabel *lblDeviceName5;

@property (nonatomic, retain) PCSockets* m_pcs;
@property (nonatomic, retain) PersistentInfo* m_persistInfo;

@property (nonatomic, retain) UITextView  *textViewDebug;
@property (nonatomic, retain) UITextField *textTest;


- (IBAction)btnON1;
- (IBAction)btnOFF1;
- (IBAction)btnON2;
- (IBAction)btnOFF2;
- (IBAction)btnON3;
- (IBAction)btnOFF3;
- (IBAction)btnON4;
- (IBAction)btnOFF4;
- (IBAction)btnON5;
- (IBAction)btnOFF5;

- (void)updateLabels;
- (void)updateDefaultValues;

/*
#if 0
- (void)saveToUserDefaults:(NSString *)sValue : (NSString *)sKey;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave;

- (BOOL)retrieveFromUserDefaultsBool:(NSString *)sKey;

//- (NSString *)retrieveFromUserDefaults:(NSString *)sKey:(NSString *)defaultValue;
#endif
*/

//- (void)updateDefaultValues:(NSString*)defaultValue;



- (IBAction)showSettings:(id)sender;


- (void)dispatchQueueTimerResume;
- (void)dispatchQueueTimerSuspend;

- (void)updateTextViewDebugState;


//@private
- (void)_sendIt:(NSString*)msg;

@end
