//
//  X10ControllerAppDelegate.h
//  X10Controller
//
//  Created by Elvis on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;
@class FlipsideViewController;

@interface X10ControllerAppDelegate : NSObject <UIApplicationDelegate> {

    UIWindow *window;
    MainViewController *mainViewController;

	FlipsideViewController *flipsideViewController;
	
//	BOOL m_bIPAddressChanged;
//	BOOL m_bPortNumberChanged;

}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet MainViewController *mainViewController;

@property (nonatomic, retain) IBOutlet FlipsideViewController *flipsideViewController;


//-(void)switchToMainView;
//-(void)switchToFlipsideView;


@end

