//
//  X10ControllerAppDelegate.m
//  X10Controller
//
//  Created by Elvis on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NSLOG1.h"
#import "X10ControllerAppDelegate.h"
#import "MainViewController.h"
#import "FlipsideViewController.h"


@implementation X10ControllerAppDelegate

@synthesize window;
@synthesize mainViewController;
@synthesize flipsideViewController;



/*
-(void)switchToMainView {
	NSLog (@"..AppDelegate switchToMainView/mainViewController");
	[[mainViewController view] removeFromSuperview];
	[self.window addSubview:mainViewController.view];
	[self.window makeKeyAndVisible];
}


-(void)switchToFlipsideView {
	NSLog (@"..AppDelegate switchToFlipsideView/flipsideViewController");
	[[flipsideViewController view] removeFromSuperview];
	[self.window addSubview:flipsideViewController.view];
	[self.window makeKeyAndVisible];
}
*/


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
  
NSLog(@"]]]]]]]]]  >>>>>> application didFinishLaunchingWithOptions <<<< [[[[[[");		
	

m_bLog  = FALSE;
m_bLog1 = FALSE;
m_bLog2 = TRUE;
	
m_bLog_All                   = FALSE;
m_bLog_PCSockets             = FALSE;
m_bLog_MainViewController    = FALSE;
m_bLog_FlipsideViewConroller = FALSE;


	
	
    // Override point for customization after application launch.  

    // Add the main view controller's view to the window and display. 
//	[self.window addSubview:flipsideViewContr	oller.view];
/*
    // Override point for customization after application launch.
	self.flipsideViewController = [[FlipsideViewController alloc]
							      initWithNibName:@"FlipsideView" 
								  bundle:[NSBundle mainBundle]];
	
  */  
	
	[self.window addSubview:mainViewController.view];
    [self.window makeKeyAndVisible];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */

	[mainViewController dispatchQueueTimerSuspend];
	
	
NSLog(@"]]]]]]]  >>>>>> applicationWillResignActive");	


	
	
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
	
NSLog(@"]]]]]]]  >>>>>> applicationDidEnterBackground");

//[mainViewController dispatchQueueTimerSuspend];	
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
	
NSLog(@"]]]]]]]  >>>>>> applicationWillEnterForeground");

//[mainViewController dispatchQueueTimerResume];	
	
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
	
	[mainViewController dispatchQueueTimerResume];
	
//	[mainViewController updateTextViewDebugState];
	
NSLog(@"]]]]]]]  >>>>>> applicationDidBecomeActive");


	
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
	
NSLog(@"]]]]]]]  >>>>>> applicationWillTerminate");	
	
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
//    [flipsideViewController release];
	[mainViewController release];
    [window release];
    [super dealloc];
}

@end
