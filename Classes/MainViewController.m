//
//  MainViewController.m
//  X10Controller
//
//  Created by John on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

//#import "EGDtest.h"
#import "NSLOG1.h"
#import "PersistentInfo.h"
#import "X10ControllerAppDelegate.h"
#import "MainViewController.h"
#import "PCSockets.h"


#define MA2(x) _sendIt(x)
#define MA1(x) x+1

#define ME1(x) _sendIt(x)
#define ME3(x) x + 1;

#define ME4(x) [self _sendIt(x)];


extern NSString * _egd_appVersion();





// Boolean m_bLog; // located in NSLOG1.h

void test1() {
	
//	ME1("test");
	
	NSLOG(NSLog(@"test");)
	
	NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
}	




void NSLog1(char * s, ...)
{
	
}	



@implementation MainViewController


@synthesize lblBuild;

@synthesize lblDeviceName1;
@synthesize lblDeviceName2;
@synthesize lblDeviceName3;
@synthesize lblDeviceName4;
@synthesize lblDeviceName5;

@synthesize m_pcs;
@synthesize m_persistInfo;

@synthesize textViewDebug;
@synthesize textTest;

@synthesize lblStatus;

@synthesize lblConnected;
@synthesize lblConnectIPandPort;
@synthesize activityIndicator;



//#define ME2(x) [self _sendIt(x)]
	


#pragma mark -
#pragma mark - BUTTONS btnON1 ...

// _okToSendIt
- (void)_sendIt:(NSString*)msg {

	if (![m_pcs okToSend])
		return;

	[m_pcs socketSend:msg];
	
}



- (void)test2 {
//	int a = MA1(3);
	//ME2[@"ON3"];
	//[self ME2("ON3")];
	
//	[self MA2("ON3")];
	
//	int a = ME3(2+1)
	
	//	ME4(@"ON1")
	
}



- (IBAction)btnON1 {
//[activityIndicator	startAnimating];
	
	if (![m_pcs okToSend])
		return;
	
	if ([m_pcs socketSend:@"ON1"] > 0) {
	//	lblStatus.text = @"OK - ON";
	}
	
}	

- (IBAction)btnOFF1 {
//[activityIndicator	stopAnimating];
	
	if (![m_pcs okToSend])
		return;
	
	if ([m_pcs socketSend:@"OFF1"] > 0) {
		//	lblStatus.text = @"OK - ON";
	}
	
	
}

- (IBAction)btnON2 {

	[self _sendIt:@"ON2"];
/*	
	if (![m_pcs okToSend])
		return;
	
	[m_pcs socketSend:@"ON2"];
*/	
}	

- (IBAction)btnOFF2 {

	[self _sendIt:@"OFF2"];
/*	
	if (![m_pcs okToSend])
		return;
	
	[m_pcs socketSend:@"OFF2"];
*/ 
}



- (IBAction)btnON3 {
	//[self ME1("ON3")];
	[self _sendIt:@"ON3"];
}	

- (IBAction)btnOFF3 {
	[self _sendIt:@"OFF3"];	
}

- (IBAction)btnON4 {
	[self _sendIt:@"ON4"];	
}	

- (IBAction)btnOFF4 {
	[self _sendIt:@"OFF4"];	
}

- (IBAction)btnON5 {
	[self _sendIt:@"ON5"];
	
//	[self dispatchQueueTimerResume]; // debug
}	

- (IBAction)btnOFF5 {
	[self _sendIt:@"OFF5"];

//	[self dispatchQueueTimerSuspend]; // debug
}


#pragma mark - BUTTONS btnON1 ... END

//-------------------------------------------------------

/*
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	if (!val)
		return sDefault;
	
	return val;
	
	
}
*/



#pragma mark -
#pragma mark - start dispatch Queue timer


- (void) monitorConnectionStatus_PCSockets_UpdateUI {

//return;  //egd1
	
	NSLOG(NSLog(@" monitorConnectionStatus_PCSockets_UpdateUI");)

//textViewDebug.text = @"test"; //monitorConnectionStatus_PCSockets_UpdateUI";	
	

	// [NSString stringWithFormat:
	//int res = 
	
//			NSString *s1 =[self retrieveFromUserDefaults:@"textModuleName5" defaultValue:@"Bedroom 2" ];
	
	NSString *s = [m_persistInfo retrieveFromUserDefaults:@"switchDebug" defaultValue:@"Default"];
	
	BOOL b2 = [m_persistInfo retrieveFromUserDefaultsBool:@"switchDebug"];
	
	
	BOOL b = [m_pcs boolStatus];
	NSLOG(NSLog(@" monitorConnectionStatus_PCSockets_UpdateUI() = %@", b ? @"TRUE" : @"FALSE");)

	//textViewDebug.text = [NSString stringWithFormat:@" %d - PCSockets_UpdateUI() = %@", rand(), b ? @"TRUE" : @"FALSE"];	

//textViewDebug.text = [NSString stringWithFormat:@" %d - PCSockets_UpdateUI() = %@", rand(), s];		
textViewDebug.text = [NSString stringWithFormat:@" %d - PCSockets_UpdateUI() = %@", rand(), b2 ? @"ON!" : @"OFF!"];
	
	//[lblLabel1 setText:[NSString stringWithFormat:@" PCSockets#%d ", rand()]]; 	
	
	lblStatus.backgroundColor = b ? [UIColor greenColor] : [UIColor redColor];
	
	if (!b)
	{
		[activityIndicator	startAnimating];
//		[activityIndicator	stopAnimating];
    //	[activityIndicator	stopAnimating];
		lblConnected.text = @"Connecting...";
		activityIndicator.hidden = NO;
		
		m_bIOSVersionSent = FALSE;
	}		
	else
	{	
		[activityIndicator	stopAnimating];
//		[activityIndicator	startAnimating];
		lblConnected.text = @"Connected";
		activityIndicator.hidden = YES;
	}

	if (!m_bIOSVersionSent)
	{	
	
		if([m_pcs okToSend])
		{
	
// egd1 - only send once	
[self _sendIt:[NSString stringWithFormat:@"iOS v.%@",_egd_appVersion()]];
			m_bIOSVersionSent = TRUE;
		}
	}	
	
	
	int len = [m_pcs socketReceive];
	
	NSString *debugReceived = [m_pcs getString]; // getSocketReceiveString socketReceiveString
	
	NSLOG(NSLog(@" >>>>>>>>>>> socketReceive() %d", len);)
	
	NSLOG(NSLog(@" >>>>>>>>>>> NSString | socketReceive() %@", debugReceived);) //[m_pcs getString]);
	
	//textViewDebug.text = debugReceived;
	
}	


//-------------------------------------------------------


-(void) startQueueTimer {
	

//	dispatch_queue_t  myQueue = dispatch_queue_create("X10 Controller Dispatch Queue Label MainViewController", NULL);	
//	dispatch_source_t timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, myQueue);

	 m_myQueue     = dispatch_queue_create("X10 Controller Dispatch Queue Label MainViewController", NULL);	
	 m_timerSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, m_myQueue);
	
	
	dispatch_source_set_timer(m_timerSource, DISPATCH_TIME_NOW, NSEC_PER_SEC * 5.0, 0); // 2 seconds

	dispatch_source_set_event_handler(m_timerSource, ^{ 
		dispatch_async(dispatch_get_main_queue(), ^{ 
			[self monitorConnectionStatus_PCSockets_UpdateUI]; }); });

	m_bTimerSourceResumed = FALSE;
	[self dispatchQueueTimerResume];
	
	
//	[m_pcs dispatchQueueTimerResume];
	
//	dispatch_resume(m_timerSource);
	
} // startQueueTimer	




// We'll use this for App State management
- (void)dispatchQueueTimerResume {

	[m_pcs dispatchQueueTimerResume];	
	
	if ((m_myQueue == NULL) || (m_bTimerSourceResumed == TRUE))
		return;

	dispatch_resume(m_timerSource);
	m_bTimerSourceResumed = TRUE;
	
//[m_pcs dispatchQueueTimerResume];
	
//	dispatch_resume(m_timerSource);		
	
}	

- (void)dispatchQueueTimerSuspend {

	[m_pcs dispatchQueueTimerSuspend];	
	
	if ((m_myQueue == NULL) || (m_bTimerSourceResumed == TRUE))
		return;

	dispatch_suspend(m_timerSource);
	m_bTimerSourceResumed = TRUE;
	
}	


#pragma mark -
#pragma mark - NSUserDefaults - Persistent Info = Windows System Registry

//-------------------------------------------------------

/*
#if 0


- (void)saveToUserDefaults:(NSString *)sValue : (NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setObject:sValue forKey:sKey];
        [standardUserDefaults synchronize];
    }
}




- (NSString *)retrieveFromUserDefaults:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	return val;
}




- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	if (!val)
		return sDefault;
	
	return val;

	
}


- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave
{

	if ( (bSave) && ([self retrieveFromUserDefaults:sKey] == NULL) )
		[self saveToUserDefaults:sDefault:sKey];
	
	
	return [self retrieveFromUserDefaults:sKey  defaultValue:sDefault];
	
}



- (void)saveToUserDefaults:(NSString *)myString
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setObject:myString forKey:@"X10ControllerSocketIP"];
        [standardUserDefaults synchronize];
    }
}

- (NSString *)retrieveFromUserDefaults
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:@"X10ControllerSocketIP"];
	
	return val;
}



- (BOOL)retrieveFromUserDefaultsBool:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL val = NO;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults boolForKey:sKey];
	
	return val;
}





- (NSString *)retrieveFromUserDefaults3:(NSString *)sKey  test:(NSString *)sDefault
{
	
	return sKey;
	
}

#endif

*/

//-------------------------------------------------------

#pragma mark -
#pragma mark - UIViewController BEGIN

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
	[super viewDidLoad];

	m_bLog = m_bLog_MainViewController;	
	
	NSLOG(NSLog(@"   >>>>>> MainViewController viewDidLoad");)
	
NSLOG(NSLog(@"   >>> default value = %@",[m_persistInfo retrieveFromUserDefaults:@"textIPAddress"]);)

	m_persistInfo = [[PersistentInfo alloc] init];
	
	
	
	
	// Initialize
	
	
	// bIsFirstRunInitialized
	
	
	
//	[m_persistInfo saveToUserDefaultsBool:TRUE:@"iX10Control_FirstTimeRun001"];
	
	m_bIsFirstRunInitialized = [m_persistInfo retrieveFromUserDefaultsBool:@"iX10Control_IsFirstRunInitialized013"];
	
	if (!m_bIsFirstRunInitialized) // if NOT first run initialized
	{
		[m_persistInfo saveToUserDefaultsBool:TRUE:@"iX10Control_IsFirstRunInitialized013"];
	/*
		[m_persistInfo saveToUserDefaults:@"Room1":@"textModuleName1"];
		[m_persistInfo saveToUserDefaults:@"Room2":@"textModuleName2"];		
		[m_persistInfo saveToUserDefaults:@"Room3":@"textModuleName3"];
		[m_persistInfo saveToUserDefaults:@"Room4":@"textModuleName4"];		
		[m_persistInfo saveToUserDefaults:@"Room5":@"textModuleName5"];	
	*/	

		
	// ip address and port number ?	
		[m_persistInfo saveToUserDefaults:@STR_IX10_DEFAULT_IP_ADDRESS:@"textIPAdress"];  // @"192.168.1.100"
		[m_persistInfo saveToUserDefaults:@STR_IX10_DEFAULT_PORT_NUMBER:@"textPortNumber"];		
		
		
		
		for (int i = 1; i < (INT_IX10_NUM_BUTTONS+1) ; i++) // 1..5
		{
			
			//textHouseLetter1
			NSString *sHouseLetter = [NSString stringWithFormat:@"textHouseLetter%d", i];
			[m_persistInfo saveToUserDefaults:@"A":sHouseLetter];
			
			
			//textModuleNumber1
			NSString *sModuleNumber = [NSString stringWithFormat:@"textModuleNumber%d", i];
			[m_persistInfo saveToUserDefaults:[NSString stringWithFormat:@"%d",i]:sModuleNumber];  // [@(myInt) stringValue]
					
			
			NSString *sVal = [NSString stringWithFormat:@"Room%d", i];
			NSString *sKey = [NSString stringWithFormat:@"textModuleName%d", i];
			[m_persistInfo saveToUserDefaults:sVal:sKey];
				
		}
		
		[m_persistInfo saveToUserDefaults:@"A":@"textHouseLetterDefault"];
		
		
//	[NSString stringWithFormat:@" %d - PCSockets_UpdateUI() = %@", rand(), b2 ? @"ON!" : @"OFF!"];	
		
		
		
		
	}
	
	
	
	
	
//	[m_persistInfo saveToUserDefaultsBool:TRUE:@"switchDebug"];
	
//	[self saveToUserDefaults:@"sDefault":@"sKey"];
	
	
	lblBuild.textColor       = [UIColor blackColor];
	lblBuild.backgroundColor = [UIColor grayColor];
//	lblBuild.text = _egd_appVersion(); //@"build 000";	
	lblBuild.text = [NSString stringWithFormat:@" v.%@ ",_egd_appVersion()]; //@"build 000";
	
	m_bIOSVersionSent = FALSE;
	
	//[activityIndicator	stopAnimating];
	[activityIndicator	startAnimating];
	
// TODO: egd1  [self updateDefaultValues];
	
//	[self updateLabels];
	[self updateDefaultValues];
	
/*	
	lblDeviceName1.text = [self retrieveFromUserDefaults:@"textModuleName1"];
	lblDeviceName2.text = [self retrieveFromUserDefaults:@"textModuleName2"];
	lblDeviceName3.text = [self retrieveFromUserDefaults:@"textModuleName3"];	
*/

	
	// PCSockets
	m_pcs = [[PCSockets alloc] init];
	

	
	
	
	if ([m_sIPAddress length] == 0)
		m_sIPAddress = @STR_IX10_DEFAULT_IP_ADDRESS; // default value
	
	if ([m_sPortNumber length] == 0)
		m_sPortNumber = @STR_IX10_DEFAULT_PORT_NUMBER;  // default value
	
//int aa =	[NSString int:@"123"];

	lblConnectIPandPort.text = [NSString stringWithFormat:@"%@:%@",m_sIPAddress,m_sPortNumber];
	
//	[m_pcs setIPAddress:@"192.168.1.105"];
	[m_pcs setIPAddress:m_sIPAddress];

	//[m_pcs setPort:11000];
	[m_pcs setPort:[m_sPortNumber intValue]]; // conversion | convert | NSString to int
	
	[m_pcs connectAndOpen];
	
//[self _sendIt:[NSString stringWithFormat:@"iOS v.%@",_egd_appVersion()]];
	
//	[m_pcs socketSend:lblVer
	
	//	[m_pcs socketConnect];
	//	[m_pcs socketOpen];
	
	
	m_myQueue = NULL;
	
	// PCSockets - check status, update UI
	[self startQueueTimer];	
	
// TODO: egd1	retrieveFromUserDefaultsBool
	BOOL b1 = [m_persistInfo retrieveFromUserDefaultsBool:@"switchDebug"];
	
	textViewDebug.hidden = ![m_persistInfo retrieveFromUserDefaultsBool:@"switchDebug"];

}



- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller {
 
	NSLOG(NSLog(@" MainViewController.m:flipsideViewControllerDidFinish");)
//NSLog(@"   flipsideViewControllerDidFinish");	
//	[self updateLabels];
	[self updateDefaultValues];

	lblConnectIPandPort.text = [NSString stringWithFormat:@"%@:%@",m_sIPAddress,m_sPortNumber];	

	[m_pcs setIPAddress:m_sIPAddress];
	[m_pcs setPort:[m_sPortNumber intValue]];

	
	[self dismissModalViewControllerAnimated:YES];

	
	// if IP Address or port number changed
	if ((m_sIPAddress != m_sIPAddressOld) || (m_sPortNumber != m_sPortNumberOld))
	{
		NSLOG(NSLog(@"   IP Address or port number changed");)
		[m_pcs connectAndOpen];
	}	
	
	
	[self updateTextViewDebugState];
	
}




- (IBAction)showSettings:(id)sender {    

	FlipsideViewController *controller = [[FlipsideViewController alloc] initWithNibName:@"FlipsideView" bundle:nil];
	controller.delegate1 = self;
	
	controller.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self presentModalViewController:controller animated:YES];

	
	NSString *test = controller.textIPAddress.text;
	
NSLOG(NSLog(@"test %@",test);)

//NSLog(@"test %@",test);	
	
	m_sIPAddressOld  = controller.textIPAddress.text;
	m_sPortNumberOld = controller.textPortNumber.text;
	
	[controller release];

	
/*	
	X10ControllerAppDelegate *appDelegate = 
	(X10ControllerAppDelegate*)[[UIApplication sharedApplication] delegate];
	[[self view] removeFromSuperview];
	[appDelegate switchToFlipsideView];	
	
*/	
	
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc. that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	NSLog(@"MainViewController.m \n viewDidUnload()");
	
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations.
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
	
	if (m_persistInfo != NULL)
	{	
		[m_persistInfo dealloc];
		
		//NSLOG(NSLog(@"MainViewController.m \n  if (m_persistInfo != NULL)  [m_persistInfo dealloc];");)
		NSLog(@"MainViewController.m \n  if (m_persistInfo != NULL)  [m_persistInfo dealloc];");
	}
	
    [super dealloc];
}



#pragma mark - UIViewController END



#pragma mark - 
#pragma mark - UITextField

//textFieldShouldBeginEditing
//textFieldShouldEndEditing


- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	
	NSLOG(NSLog(@"textFieldShouldReturn");)
	
	[theTextField resignFirstResponder];	

	return TRUE;
	
}	

- (BOOL)textFieldShouldBeginEditing:(UITextField *)theTextField {
	
	NSLOG(NSLog(@"textFieldShouldBeginEditing");)
	
	
	return TRUE;	
	
}	

- (BOOL)textFieldShouldEndEditing:(UITextField *)theTextField {
	
	NSLOG(NSLog(@"textFieldShouldEndEditing");)
	
	// When the focus is changed from say textField1 to another textField2, then
	//  theTextField is textField1, so m_currentTextField != theTextField;
	
	//if (theTextField == textIPAddress)
	//	NSLOG(NSLog(@"textIPAddress");)
	
	// Save he current IP Address to the persistent storage
	
	//	[self saveToUserDefaults:textIPAddress.text];
	
	
	return TRUE;
}



#pragma mark -
#pragma mark - interface UIResponder : NSObject

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	NSLOG(NSLog(@"touchesBegan");)
	
	
	// Dismiss the keyboard when the view outside the text field is touched.
//	[m_currentTextField resignFirstResponder];	
	
	
	/*	
	 if (m_currentTextField == textField) 
	 NSLOG(NSLog(@"textField");)
	 else if (m_currentTextField == textIPAddress)
	 NSLOG(NSLog(@"textIPAddress");)	
	 */	
	/*	
	 THIS CODE APPEARS TO DESTABILIZE THE APP!!!
	 
	 // update current IP Address when touched outside a text field
	 if (m_currentTextField == textIPAddress) {
	 
	 m_sIPAddress = lblIPAddress.text = textIPAddress.text;
	 
	 }
	 
	 */	
	
	
	
	// Dismiss the keyboard when the view outside the text field is touched.
    //[textField resignFirstResponder];
    //[textIPAddress resignFirstResponder];
	
	//	[m_currentTextField resignFirstResponder];
	
    // Revert the text field to the previous value.
	//    textField.text = self.string1; 
	//m_currentTextField.text = sTextFieldSaveForRevert;
	
	//egd1    [super touchesBegan:touches withEvent:event];
	
	//[super touchesBegan:touches withEvent:event];	
	
}


#pragma mark -
#pragma mark - STUFF



- (void)updateLabels {
	
	// if userDefault is nil then update user default with default values,
	// then update label texts

//[m_persistInfo test1a];
/*
	lblDeviceName1.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName1" defaultValue:@"Bedroom 1" saveDefault:TRUE];
	lblDeviceName2.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName2" defaultValue:@"Garage" saveDefault:TRUE];
	lblDeviceName3.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName3" defaultValue:@"Kitchen" saveDefault:TRUE];
	lblDeviceName4.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName4" defaultValue:@"Living Room" saveDefault:TRUE];

//	NSString * test = [m_persistInfo retrieveFromUserDefaults2:@"textModuleName5" default:@"room5a" ];
	
	lblDeviceName5.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName5" defaultValue:@"Bedroom 2" saveDefault:TRUE];
*/
	
	NSString *s1 = [m_persistInfo retrieveFromUserDefaults:@"textModuleName1"];
	
	
	lblDeviceName1.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName1"];
	lblDeviceName2.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName2"];
	lblDeviceName3.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName3"];	
	lblDeviceName4.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName4"];
	lblDeviceName5.text = [m_persistInfo retrieveFromUserDefaults:@"textModuleName5"];	
	
}		





- (void)updateDefaultValues {

	m_sIPAddress  = [m_persistInfo retrieveFromUserDefaults:@"textIPAddress"  defaultValue:@STR_IX10_DEFAULT_IP_ADDRESS saveDefault:TRUE];
 	m_sPortNumber = [m_persistInfo retrieveFromUserDefaults:@"textPortNumber" defaultValue:@STR_IX10_DEFAULT_PORT_NUMBER saveDefault:TRUE];
//m_sPortNumber = [self retrieveFromUserDefaults:@"textPortNumber" defaultValue:@"11000"];	
	//NSLog(@"updateDefaultValues m_sPortNumber = %@",m_sPortNumber);
	[self updateLabels];
}


- (void)updateTextViewDebugState
{

//	test4();
	
		textViewDebug.hidden = ![m_persistInfo retrieveFromUserDefaultsBool:@"switchDebug"];
	
}	


@end
