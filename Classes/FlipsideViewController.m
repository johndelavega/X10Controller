//
//  FlipsideViewController.m
//  X10Controller
//
//  Created by Elvis on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

//#import "EGDtest.h"
#import "ValidEntry.h"  // currently not used at runtime, can be used as a reference for creating a new class
#import "PersistentInfo.h"
#import "X10ControllerAppDelegate.h"
#import "FlipsideViewController.h"


@implementation FlipsideViewController

@synthesize delegate1;
@synthesize scrollView;

@synthesize m_closeKeyboard; // Referencing Outlets
@synthesize m_done;

@synthesize textIPAddress;
@synthesize textPortNumber;


@synthesize textHouseLetter1;
@synthesize textModuleNumber1;
@synthesize textModuleName1;

@synthesize textHouseLetter2;
@synthesize textModuleNumber2;
@synthesize textModuleName2;

@synthesize textHouseLetter3;
@synthesize textModuleNumber3;
@synthesize textModuleName3;

@synthesize textHouseLetter4;
@synthesize textModuleNumber4;
@synthesize textModuleName4;

@synthesize textHouseLetter5;
@synthesize textModuleNumber5;
@synthesize textModuleName5;

@synthesize textHouseLetterDefault;

//@synthesize m_arTextFieldKeyNames;

@synthesize m_persistInfo;

@synthesize m_validEntry;

@synthesize m_pSetHouseLetter;
@synthesize m_pSetModuleNumber;

@synthesize m_pSet1;

//@synthesize m_sBeforeEdit2;
@synthesize m_sBeforeEdit;
@synthesize m_sCurrentTextFieldTextBeforeEdit;

// testing
@synthesize m_ma;

@synthesize switchDebug;


#if 0

struct VarName {
	id id;
	NSString *name;
};
typedef struct VarName VarName;



NSString *ar1a [] = { @"one", @"two" };

#endif



// [NSSet setWithObjects:someData, aValue, aString, nil];
NSSet *m_pSet2;


NSString *m_sBeforeEdit2;


/*
static NSString *m_strStatus [] = { 
	@"kCFStreamStatusNotOpen", 


 */
 

/*
NSObject *m_strStatus2 [] = { 
	textIPAddress,
	@"kCFStreamStatusNotOpen", 
};
*/


#pragma mark -
#pragma mark -


- (void)initTextFieldKeyNames { // for user default values

	
	// self.  = persistent assignment	
	self.m_pSetHouseLetter  = [NSSet setWithObjects:textHouseLetter1, textHouseLetter2, textHouseLetter3, textHouseLetter4, textHouseLetter5, textHouseLetterDefault, nil];
	self.m_pSetModuleNumber = [NSSet setWithObjects:textModuleNumber1, textModuleNumber2, textModuleNumber3, textModuleNumber4, textModuleNumber5, nil];
	
	
	NSArray *arTextHouseLetter  = [[NSArray alloc] initWithObjects:textHouseLetter1, textHouseLetter2, textHouseLetter3, textHouseLetter4, textHouseLetter5, nil];
	NSArray *arTextModuleNumber = [[NSArray alloc] initWithObjects:textModuleNumber1, textModuleNumber2, textModuleNumber3, textModuleNumber4, textModuleNumber5, nil];
	NSArray *arTextModuleName   = [[NSArray alloc] initWithObjects:textModuleName1, textModuleName2, textModuleName3, textModuleName4, textModuleName5, nil];
								  
	
	m_arTextFieldKeyNames = [[NSMutableArray alloc] initWithCapacity:200];

/*	
	[m_arTextFieldKeyNames addObject:(id)textIPAddress];
	[m_arTextFieldKeyNames addObject:(id)@"textIPAddress"];
	[m_arTextFieldKeyNames addObject:(id)textPortNumber];	
	[m_arTextFieldKeyNames addObject:(id)@"textPortNumber"];
*/
	
	[m_arTextFieldKeyNames addObject:textIPAddress];
	[m_arTextFieldKeyNames addObject:@"textIPAddress"];
	[m_arTextFieldKeyNames addObject:@STR_IX10_DEFAULT_IP_ADDRESS]; // @"192.168.1.100"
	[m_arTextFieldKeyNames addObject:textPortNumber];
	[m_arTextFieldKeyNames addObject:@"textPortNumber"];
	[m_arTextFieldKeyNames addObject:@STR_IX10_DEFAULT_PORT_NUMBER];

	
	
	for (int i = 1; i < (INT_IX10_NUM_BUTTONS+1) ; i++) // 1..5
	{

		[m_arTextFieldKeyNames addObject:[arTextHouseLetter objectAtIndex:i-1]];	
		[m_arTextFieldKeyNames addObject:[NSString stringWithFormat:@"textHouseLetter%d", i]];
		[m_arTextFieldKeyNames addObject:@"A"];
		[m_arTextFieldKeyNames addObject:[arTextModuleNumber objectAtIndex:i-1]];	
		[m_arTextFieldKeyNames addObject:[NSString stringWithFormat:@"textModuleNumber%d", i]];
		[m_arTextFieldKeyNames addObject:[NSString stringWithFormat:@"%d", i]];
		[m_arTextFieldKeyNames addObject:[arTextModuleName objectAtIndex:i-1]];	
		[m_arTextFieldKeyNames addObject:[NSString stringWithFormat:@"textModuleName%d", i]];
		[m_arTextFieldKeyNames addObject:[NSString stringWithFormat:@"Room%d", i]];		
		
	}
	
	
	[arTextHouseLetter  dealloc];
	[arTextModuleNumber dealloc];
	[arTextModuleName   dealloc];
	
	
/*	
	[m_arTextFieldKeyNames addObject:textHouseLetter1];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetter1"];
	[m_arTextFieldKeyNames addObject:@"A"];
	[m_arTextFieldKeyNames addObject:textModuleNumber1];	
	[m_arTextFieldKeyNames addObject:@"textModuleNumber1"];
	[m_arTextFieldKeyNames addObject:@"1"];
	[m_arTextFieldKeyNames addObject:textModuleName1];	
	[m_arTextFieldKeyNames addObject:@"textModuleName1"];
	[m_arTextFieldKeyNames addObject:@"Master Bedroom"];
	
	[m_arTextFieldKeyNames addObject:textHouseLetter2];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetter2"];
	[m_arTextFieldKeyNames addObject:@"A"];
	[m_arTextFieldKeyNames addObject:textModuleNumber2];	
	[m_arTextFieldKeyNames addObject:@"textModuleNumber2"];
	[m_arTextFieldKeyNames addObject:@"2"];
	[m_arTextFieldKeyNames addObject:textModuleName2];	
	[m_arTextFieldKeyNames addObject:@"textModuleName2"];
	[m_arTextFieldKeyNames addObject:@"Bedroom 1"];
	
	[m_arTextFieldKeyNames addObject:textHouseLetter3];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetter3"];
	[m_arTextFieldKeyNames addObject:@"A"];
	[m_arTextFieldKeyNames addObject:textModuleNumber3];	
	[m_arTextFieldKeyNames addObject:@"textModuleNumber3"];
	[m_arTextFieldKeyNames addObject:@"3"];
	[m_arTextFieldKeyNames addObject:textModuleName3];	
	[m_arTextFieldKeyNames addObject:@"textModuleName3"];
	[m_arTextFieldKeyNames addObject:@"Bedroom 2"];
	
	[m_arTextFieldKeyNames addObject:textHouseLetter4];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetter4"];
	[m_arTextFieldKeyNames addObject:@"A"];
	[m_arTextFieldKeyNames addObject:textModuleNumber4];	
	[m_arTextFieldKeyNames addObject:@"textModuleNumber4"];
	[m_arTextFieldKeyNames addObject:@"4"];
	[m_arTextFieldKeyNames addObject:textModuleName4];	
	[m_arTextFieldKeyNames addObject:@"textModuleName4"];
	[m_arTextFieldKeyNames addObject:@"Kitchen"];
	
	[m_arTextFieldKeyNames addObject:textHouseLetter5];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetter5"];
	[m_arTextFieldKeyNames addObject:@"A"];
	[m_arTextFieldKeyNames addObject:textModuleNumber5];	
	[m_arTextFieldKeyNames addObject:@"textModuleNumber5"];
	[m_arTextFieldKeyNames addObject:@"5"];
	[m_arTextFieldKeyNames addObject:textModuleName5];	
	[m_arTextFieldKeyNames addObject:@"textModuleName5"];
	[m_arTextFieldKeyNames addObject:@"Garage"];
*/	
	[m_arTextFieldKeyNames addObject:textHouseLetterDefault];	
	[m_arTextFieldKeyNames addObject:@"textHouseLetterDefault"];
	[m_arTextFieldKeyNames addObject:@"A"];	
	
	// current count is 36? objects
	
	
	/*  // 
	 // NSArray *m_arTextFieldKeyNames	
	m_arTextFieldKeyNames = [NSMutableArray arrayWithObjects: 
							 textIPAddress,@"textIPAddress",
							 textPortNumber,@"textPortNumber", 
							 nil];
*/

/*
	
	m_arTextFieldKeyNames = [NSMutableArray arrayWithObjects: 
							 @"textIPAddress",@"textIPAddress",
							 @"textIPAddress",@"textPortNumber", 
							 nil];
	
*/
	
//	int c1 = m_arTextFieldKeyNames.count;
	
}	





// var = variable name
- (NSString*)textFieldKeyName:(id)var {

/*
 - (NSUInteger)count;
 - (id)objectAtIndex:(NSUInteger)index;	
*/	

	int cnt = m_arTextFieldKeyNames.count;
	
	
	for (int j=0 ; j<cnt ; j=j+3)
	{	
		id v = [m_arTextFieldKeyNames objectAtIndex:j]; 
	
		if (v == var)
			return [m_arTextFieldKeyNames objectAtIndex:j+1];
	}	
	
	return @"NOT FOUND";
	
}	


- (NSString*)textFieldDefaultValue:(id)var {
	
	int cnt = m_arTextFieldKeyNames.count;
	
	
	for (int j=0 ; j<cnt ; j=j+3)
	{	
		id v = [m_arTextFieldKeyNames objectAtIndex:j]; 
		
		if (v == var)
			return [m_arTextFieldKeyNames objectAtIndex:j+2];
	}	
	
	return @"NOT FOUND";
	
}	



- (void)setTextFieldValuesFromUserDefaults {

	int cnt = m_arTextFieldKeyNames.count;

	for (int j=0 ; j<cnt ; j=j+3)
	{	
		UITextField* tf = [m_arTextFieldKeyNames objectAtIndex:j]; 
		
		tf.text = [m_persistInfo retrieveFromUserDefaults:[self textFieldKeyName:tf]];

	}	
	
}	



// code snippets

#if 0

//	int c = [m_arTextFieldKeyNames count];

//	tf.text = @"test";
// 	m_sPortNumber = [self retrieveFromUserDefaults:@"textPortNumber" defaultValue:@"11000" saveDefault:TRUE];		
//	NSString *s1 = [m_persistInfo retrieveFromUserDefaults:[self textFieldKeyName:tf] defaultValue:[self textFieldDefaultValue:tf] saveDefault:TRUE];
//	tf.text = [m_persistInfo retrieveFromUserDefaults:[self textFieldKeyName:tf] defaultValue:[self textFieldDefaultValue:tf] saveDefault:TRUE];
//	textIPAddress.text = [self retrieveFromUserDefaults:@"textIPAddress"];	




- (NSString*)varNameString2:(id)var {
	

	
	
	NSObject *m_strStatus1a[] = { 
		textIPAddress,
		@"kCFStreamStatusNotOpen", 
	};
	
	
	
	
	VarName vn = { textIPAddress, @"textIPAddress" };
	//, textPortNumber,@"textIPAddress" };

	
	VarName vn1 [] = { 
		{ textIPAddress,  @"textIPAddress" },
		{ textPortNumber, @"textPortNumber" }	
	};
	
	
	
	
//	var == textIPAddress ? 
	
	if  (var == textIPAddress)
		return @"textIPAddress";
	
	
	return @"varNameString_TEST";
	
}	

#endif




#pragma mark -
#pragma mark - NSUserDefaults - Persistent Info = Windows System Registry

//-------------------------------------------------------

/*

- (void)saveToUserDefaults:(NSString *)sValue : (NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setObject:sValue forKey:sKey];
        [standardUserDefaults synchronize];
    }
}



- (void)saveToUserDefaultsBool:(BOOL)bValue : (NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setBool:bValue forKey:sKey];
        [standardUserDefaults synchronize];
    }
}



- (NSString *)retrieveFromUserDefaults:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	return val;
}



- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	if (!val)
		return sDefault;
	
	return val;
	
	
}



- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave
{
	
	if ( (bSave) && ([self retrieveFromUserDefaults:sKey] == NULL) )
		[self saveToUserDefaults:sDefault:sKey];
	
	
	return [self retrieveFromUserDefaults:sKey  defaultValue:sDefault];
	
}




- (BOOL)retrieveFromUserDefaultsBool:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL val = NO;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults boolForKey:sKey];
	
	return val;
}

*/


#pragma mark - 
#pragma mark - UITextField

//textFieldShouldBeginEditing
//textFieldShouldEndEditing

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	NSLOG(NSLog(@"textFieldDidBeginEditing");)
//	int c1 = m_arTextFieldKeyNames.count;
	// m_currentTextField is the current text field when moving from another text field
	m_currentTextField = textField;
	
	self.m_sCurrentTextFieldTextBeforeEdit = textField.text; // "out of scope" problem! use self. prefix
	
	
	NSString *desc1 = m_currentTextField.description;	
	
	NSLOG(NSLog(@"  >>> NSObject::description %@",desc1);)
	
/*	
>>> NSObject::description <UITextField: 0x4b6c620; frame = (16 160; 25 31); text = 'A'; 
 clipsToBounds = YES; opaque = NO; autoresize = RM+BM; layer = <CALayer: 0x4b6c730>>
 */
	
//	NSString *string = [m_currentTextField valueForKey:@"stringProperty"];	
//	NSLOG(NSLog(@"  >>> stringProperty %@",string);		
	
	
	m_done.hidden = YES;
	
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)theTextField {

	NSLOG(NSLog(@"textFieldShouldBeginEditing");)
	//m_currentTextField = theTextField;
	
//	m_currentTextField = theTextField;	
		m_closeKeyboard.hidden = NO;
	
	if (theTextField == textIPAddress)
		NSLOG(NSLog(@"textIPAddress");)
	
	/*	
	 sTextFieldSaveForRevert = m_currentTextField.text;
	 
	 if (theTextField == textField) {
	 sTextFieldSaveForRevert = textField.text;
	 }
	 else if (theTextField == textIPAddress) {
	 sTextFieldSaveForRevert = textIPAddress.text;
	 }
	 */	
	
	
//[self uiArray];
//[self hideControls:YES];	

	
	// increase vertical content size of view scroll 
	CGRect f4 = scrollView.frame;
	NSLOG(NSLog(@"scrollView - size w:%f, h:%f",f4.size.width,f4.size.height);)
	f4.size.height *= 1.5;
	//scrollViewTest.frame = f3;
	scrollView.contentSize = f4.size;		
	
	
	// check if textField is already visible when keyboard is active and don't update y offset
	// scrollView.contentOffset.y
	
//	CGFloat y = scrollView.contentOffset.y;


  if (!m_bEditingTextField)
  {	
	
	if ( (theTextField == textHouseLetter3)  
	   || (theTextField == textModuleNumber3) 
       || (theTextField == textModuleName3) )	
	{
/*
		CGRect f = m_frame; // = viewScroll.frame;
		f.origin.y -= 35.0; // scroll or move up
		scrollView.frame = f;
*/
		
		// TODO:
		// test scrollView.contentOffset.y here then update if contentOffset if text field is not visible
		
		CGPoint pt = scrollView.contentOffset;
		pt.y += 35.0;
		scrollView.contentOffset = pt;
	
	
	}
	
	else if ( (theTextField == textHouseLetter4)
		|| (theTextField == textModuleNumber4) 
		|| (theTextField == textModuleName4) )	
	{
/*
		CGRect f = m_frame;
		f.origin.y -= (35.0 + 44.0); // scroll or move up
		scrollView.frame = f;
*/
		CGPoint pt = m_offset;
		pt.y += (35.0 + 44.0);
		scrollView.contentOffset = pt;
		
	}	
	
	else if ( (theTextField == textHouseLetter5)
			 || (theTextField == textModuleNumber5) 
			 || (theTextField == textModuleName5) )	
	{
/*		CGRect f = m_frame;
		f.origin.y -= (35.0 + 44.0 + 44.0); // scroll or move up
		scrollView.frame = f;
*/
		CGPoint pt = m_offset;
		pt.y += (35.0 + 44.0 + 44.0);
		scrollView.contentOffset = pt;		
	}		
	
	else if (theTextField == textHouseLetterDefault)
	{
/*		CGRect f = m_frame;
		f.origin.y -= (35.0 + 44.0 + 44.0 + 50.0); // scroll or move up
		scrollView.frame = f;
*/
		CGPoint pt = m_offset;
		pt.y += (35.0 + 44.0 + 44.0 + 50.0);
		scrollView.contentOffset = pt;			
	}		
	
  } // if (!m_bEditingTextField)
	
	m_bEditingTextField = YES;
	
	return YES;
}	



- (BOOL)textFieldShouldEndEditing:(UITextField *)theTextField {
	
	NSLOG(NSLog(@"textFieldShouldEndEditing");)
	
	// When the focus is changed from say textField1 to another textField2, then
	//  theTextField is textField1, so m_currentTextField != theTextField;
	
	 if (theTextField == textIPAddress)
		NSLOG(NSLog(@"textIPAddress");)
	
	// Save he current IP Address to the persistent storage
	
//	[self saveToUserDefaults:textIPAddress.text];
	

	return YES;
}




- (NSString*)varNameString:(id)var {

	
//	return [self textFieldKeyName:var];
	
	NSString *s = [self textFieldKeyName:var];
	
	return s;
	
/*	
	if  (var == textIPAddress)
		return @"textIPAddress";
	
	
	return @"varNameString_TEST";
*/
}	




- (void)textFieldDidEndEditing:(UITextField*)textField {

	NSLOG(NSLog(@"textFieldDidEndEditing = %@",textField.text);)
	
	NSLOG(NSLog(@"textFieldDidEndEditing m_currentTextField = %@", m_currentTextField.text);)	
	
	NSLOG(NSLog(@" >>>>> textFieldDidEndEditing varNameString  = %@",[self varNameString:textField]);)

	// restore previouse value if text field is empty for module number and house letter text fields only
	if ( (textField.text.length == 0) 
		&& ([m_pSetModuleNumber containsObject:textField] 
		||  [m_pSetHouseLetter  containsObject:textField]) )
	{	
		textField.text = m_sCurrentTextFieldTextBeforeEdit;
	}
	

//	[m_validEntry isValidEntry3]; // - (void)isValidEntry3 
//	[m_validEntry isValidEntry2:textHouseLetter1];
	
	
	[m_persistInfo saveToUserDefaults:textField.text:[self varNameString:textField]];	
	

}	


- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	
	NSLOG(NSLog(@"textFieldShouldReturn");)

	[theTextField resignFirstResponder];
	
/*	
	// When the user presses return, take focus away from the text field so that the keyboard is dismissed.
	if (theTextField == m_currentTextField) {
		[m_currentTextField resignFirstResponder];
		
        // Invoke the method that changes the greeting.
        [self updateString];
	}
*/	
	if (theTextField == textIPAddress) {
		//m_sIPAddress = textIPAddress.text;
		//lblIPAddress.text = m_sIPAddress;
//		m_sIPAddress = lblIPAddress.text = textIPAddress.text;
//		[m_pcs setIPAddress:m_sIPAddress];
	}

	// retore original values
	scrollView.frame = m_frame;	
	scrollView.contentOffset = m_offset;
	scrollView.contentSize = m_contentSize;
	
	m_bEditingTextField = NO;
	
	m_closeKeyboard.hidden = YES;	
	
	return YES;
}



-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
// textField.text is the current string value
// string is what's typed in
	
	
NSLog(@" @@@@@@ -(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:  text %@,  replacementString=%@",textField.text,string);
	
	if ([m_pSetHouseLetter containsObject:textField])
	{	

		NSLog(@" @@@@@@ HOUSE LETTER TEXT FIELD !!!");
	
		// "abcdefgABCDEFG" testing
		// "abcdefghijklmnopABCDEFGHIJKLMNOP"   X10 A..P 16 chars
		
		// Only characters in the NSCharacterSet you choose will be insertable.
		NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopABCDEFGHIJKLMNOP"] invertedSet];
		NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
		
			
		if ([string isEqualToString:filtered])
			textField.text = [string uppercaseString];
		
		return NO;
		
	}
	else if ([m_pSetModuleNumber containsObject:textField])
	{
	
		NSLog(@" @@@@@@ MODULE NUMBER TEXT FIELD !!! text len %d, range len %d",textField.text.length, range.length );	
		
 
		// Only characters in the NSCharacterSet you choose will be insertable.
		NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
		NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
		
	
		// BEGIN filter out int value > 16

		if ((textField.text.length == 0) && (string.intValue == 0)) // fileter out 1st char 0 
			return NO;
	
		if (textField.text.length == 1)
		{	
			// backspace char value escape sequence
			
			// detecting backspace - what other conditions besides backspace?
			// string.length is 0
			// [string isEqualToString:@""] is true
			
				
			// (string.length > 0) -> backspace is not pressed
			
			// if the 1st char int value is > 1 then any 2nd char num value will be > 16
			// filter all except backspace char
			if ((textField.text.intValue > 1) && (string.length > 0)) // if backspace is not pressed
				return NO;
			
			// so we have 1st char num value should be 1 at this point, only accept 2nd char num value < 7
			if (string.intValue > 6)			
				return NO;	
		}
			
		// END filter out int value > 16

		
		
		// limit to valid chars, numbers only in this case
		if (![string isEqualToString:filtered])
			return NO;
		
		// limit to 2 chars
		if (textField.text.length >= 2 && range.length == 0)
			return NO;

	
	}  // else if ([m_pSetModuleNumber containsObject:textField])


	// no filter, normal text entry at this piont
	return YES;
	
}



#pragma mark -
#pragma mark - interface UIResponder : NSObject

// NOT WORKING HERE, BUT WORKS In MainViewController !!!

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	
	NSLOG(NSLog(@"touchesBegan");)
	
	
	// Dismiss the keyboard when the view outside the text field is touched.
	[m_currentTextField resignFirstResponder];	
	
	
/*	
	if (m_currentTextField == textField) 
		NSLOG(NSLog(@"textField");)	
	else if (m_currentTextField == textIPAddress)
		NSLOG(NSLog(@"textIPAddress");)	
*/	
	/*	
	 THIS CODE APPEARS TO DESTABILIZE THE APP!!!
	 
	 // update current IP Address when touched outside a text field
	 if (m_currentTextField == textIPAddress) {
	 
	 m_sIPAddress = lblIPAddress.text = textIPAddress.text;
	 
	 }
	 
	 */	
	
	
	
	// Dismiss the keyboard when the view outside the text field is touched.
    //[textField resignFirstResponder];
    //[textIPAddress resignFirstResponder];
	
//	[m_currentTextField resignFirstResponder];
	
    // Revert the text field to the previous value.
//    textField.text = self.string1; 
	//m_currentTextField.text = sTextFieldSaveForRevert;
	
//egd1    [super touchesBegan:touches withEvent:event];
	
//[super touchesBegan:touches withEvent:event];	
	
}




//-------------------------------------------------------

#pragma mark -
#pragma mark - UIViewController BEGIN

- (void)viewDidLoad {
    [super viewDidLoad];

	m_bLog = m_bLog_FlipsideViewConroller;	
	
	m_persistInfo = [[PersistentInfo alloc] init];
	
	m_validEntry = [[ValidEntry alloc] init];	
	
//	m_pSet1 = [[NSSet alloc] init];	
	
    self.view.backgroundColor = [UIColor viewFlipsideBackgroundColor];  
    
	textIPAddress.text = [m_persistInfo retrieveFromUserDefaults:@"textIPAddress"];

//BOOL b2 = [self retrieveFromUserDefaultsBool:@"switchDebug"];
// need to access UISwitch controll then set it to b2, done	
//[m_switchDebug setOn:b2];

	[m_switchDebug setOn:[m_persistInfo retrieveFromUserDefaultsBool:@"switchDebug"]];	

	
	m_frame  = scrollView.frame;
	m_offset = scrollView.contentOffset;
	m_contentSize = scrollView.contentSize;
	
	m_bEditingTextField = NO;
	
	//btnReleaseKeyboard.enabled = NO;
	m_closeKeyboard.hidden = YES;
	

	[self initTextFieldKeyNames];


// FIXME: egd1 fixme
// MARK: egd1 mark	// MARK:

// ???: egd1 ???
// !!!: egd1 !!!
// MARK: egd1 #warning	

#warning TEST warning	
	
// TODO: egd1 setTextFieldValuesFromUserDefaults	
	[self setTextFieldValuesFromUserDefaults];



//	NSString *s2 = textIPAddress.text;
//	NSString *s3 = textPortNumber.text;
	
	

	
//	int c1 = m_arTextFieldKeyNames.count;	
	
/*
	// increase vertical content size of view scroll 
	CGRect f4 = scrollView.frame;
	NSLOG(NSLog(@"scrollView - size w:%f, h:%f",f4.size.width,f4.size.height);
	f4.size.height *= 1.5;
	//scrollViewTest.frame = f3;
	scrollView.contentSize = f4.size;		
*/	
	// scrollView.contentInset.top = 50.0; // Lvalue
	
	NSLOG(NSLog(@" 1. viewDidLoad() scrollView - contentSize w:%f, h:%f",scrollView.contentSize.width,scrollView.contentSize.height);)
	
//	NSLOG(NSLog(@" 1. viewDidLoad() scrollView - contentSize w:%f, h:%f",scrollView.contentSize.width,scrollView.contentSize.height);)
	
//	CGPoint pt1 = scrollView.contentOffset;
	
	
}



- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	
	[m_arTextFieldKeyNames release];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	// Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)dealloc {
	
	
	if (m_validEntry != NULL)
		[m_validEntry dealloc];
		
	
	
	if (m_persistInfo != NULL)
	{	
		[m_persistInfo dealloc];
		
		NSLOG(NSLog(@"FlipsideViewController.m \n  if (m_persistInfo != NULL)  [m_persistInfo dealloc];");)
	}
	
    [super dealloc];
}


#pragma mark - UIViewController END
#pragma mark -


//-------------------------------------------------------

#pragma mark - Buttons - IBAction

- (IBAction)done:(id)sender {

	// this fixes the problem when done button is pushed/clicked during text editing
	// this call is needed before the next line : [self.delegate1 flipsideViewControllerDidFinish:self];
	[m_persistInfo saveToUserDefaults:m_currentTextField.text:[self varNameString:m_currentTextField]];		
	
//int c1 = m_arTextFieldKeyNames.count;	
	[self.delegate1 flipsideViewControllerDidFinish:self]; // MainViewController.m:flipsideViewControllerDidFinish() is called!!
	
	/*	
	 X10ControllerAppDelegate *appDelegate = 
	 (X10ControllerAppDelegate*)[[UIApplication sharedApplication] delegate];
	 [[self view] removeFromSuperview];
	 [appDelegate switchToMainView];	
	 */

//[m_persistInfo saveToUserDefaults:m_currentTextField.text:[self varNameString:m_currentTextField]];	
	
	NSLOG(NSLog(@" FlipsideViewController.m: - (IBAction)done:(id)sender ");)	
	
}


- (IBAction)btnCloseKeyboard {  // Events in Interface Builder
	
	NSLOG(NSLog(@"btnCloseKeyboard");)
	
	
	[m_currentTextField resignFirstResponder];	
	
	m_bEditingTextField = NO;
	
	m_closeKeyboard.hidden = YES;
	
	scrollView.contentSize = m_contentSize;
}	

//-------------------------------------------------------


#pragma mark -
#pragma mark - viewForZoomingInScrollView

#if 0

// UIScrollView 
// problem with zoomScale | setZoomScale
// this delegate fixes the problem
// only works with 1 object
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrlView {
	NSLOG(NSLog(@"viewForZoomingInScrollView");)
	// 
	//return viewTest; // OK!
	return [scrlView.subviews objectAtIndex:0];
	//return scrlView.view;
}

#endif

#pragma mark - viewForZoomingInScrollView



#pragma mark - 
#pragma mark - testing array controls

- (void)uiArray {
	
NSLOG(NSLog(@" uiArray");)
	
	//NSArray *a;
	
	//NSMutableArray *ma
	// local declaration appears to work also
	// NSMutableArray *ma1 = [[NSMutableArray alloc] initWithCapacity:10];

	//m_ma = [[[NSMutableArray alloc] initWithCapacity:10] autorelease]; // autorelease causes CRASH!!!
	
	m_ma = [[NSMutableArray alloc] initWithCapacity:10];	

	
	[m_ma addObject:(id)textHouseLetter1]; // CRASH!!! requires alloc, also need to associate UI control with Interface Builder IB
	[m_ma addObject:(id)textHouseLetter2];
	[m_ma addObject:(id)textHouseLetter3];
	
	
	NSUInteger cnt = m_ma.count;
	
	NSLOG(NSLog(@" array count = %d",cnt);)
	
}	



- (void)hideControls:(BOOL)hide {

NSUInteger cnt = m_ma.count;
	
	UITextField *tf;
	
	for (int i = 0; i < cnt; i++)
	{
		tf = [m_ma objectAtIndex:i];
		[tf setHidden:hide];
	}	
 //[textHouseLetter1 setHidden:hide];


}

#pragma mark - 
#pragma mark - Debug

- (IBAction)switchDebug:(id)sender {
	
//	NSLOG(NSLog(@"switchDebug");	
/*	
	BOOL bEnableDeceleration = NO;
	-(IBAction) onDeceleration:(id) sender {
		UISwitch *sch = (UISwitch*)sender;
		bEnableDeceleration = !(sch.on);
*/
	
	UISwitch *sch = (UISwitch*)sender;
	
	BOOL bSwitch = (sch.on);
	
	NSString *sSwitch = [NSString stringWithFormat:@"%@", bSwitch ? @"ON" : @"OFF"];
	
	
	//[self saveToUserDefaults:sSwitch:@"switchDebug"];	
	[m_persistInfo saveToUserDefaultsBool:bSwitch:@"switchDebug"];	


	NSLOG(NSLog(@"switchDebug - %@",sSwitch);)

	
	
	//test4();	
	
	//NSString *s1 = testStringReturn();
	
	
	//NSLog(@"testStringReturn - %@",s1);
	
}	






@end
