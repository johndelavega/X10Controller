//
//  PersistentInfo.m
//  X10Controller
//
//  Created by Elvis on 5/8/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "PersistentInfo.h"


@implementation PersistentInfo




- (void)test1a
{
	NSLog(@"@@@@@@@ test1a");
	
	int x =  1;
	
}	




- (void)saveToUserDefaults:(NSString *)sValue : (NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setObject:sValue forKey:sKey];
        [standardUserDefaults synchronize];
    }
}



- (NSString *)retrieveFromUserDefaults:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	return val;
}




- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *val = nil;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults objectForKey:sKey];
	
	if (!val)
		return sDefault;
	
	return val;
	
	
}


- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave
{
//NSString *s1 = [self retrieveFromUserDefaults:sKey];
	
	NSUInteger l1 =	[[self retrieveFromUserDefaults:sKey] length];
	BOOL b1 = [self retrieveFromUserDefaults:sKey] == NULL;
	BOOL b2 = [[self retrieveFromUserDefaults:sKey] length] == 0;
	
	if ( (bSave) && ([[self retrieveFromUserDefaults:sKey] length] == 0) )
		[self saveToUserDefaults:sDefault:sKey];
	
	
	return [self retrieveFromUserDefaults:sKey  defaultValue:sDefault];
	
}


- (void)saveToUserDefaultsBool:(BOOL)bValue : (NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
	
    if (standardUserDefaults) {
        [standardUserDefaults setBool:bValue forKey:sKey];
        [standardUserDefaults synchronize];
    }
}


- (BOOL)retrieveFromUserDefaultsBool:(NSString *)sKey
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL val = NO;
	
    if (standardUserDefaults) 
        val = [standardUserDefaults boolForKey:sKey];
	
	return val;
}




@end
