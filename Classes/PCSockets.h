//
//  PCSockets.h - PC = Persistent Connection
//  SocketApp
//
//  Created by John on 8/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
//  .\X10Controller\Classes\

#import <Foundation/Foundation.h>




/*
@protocol PCSockets <NSObject>

- (id) delegate;

@end
*/


/*
@protocol PCSocketsDelegate <NSObject>

@optional

- (void) infoOnlyProtocolMethod;

@end

@interface PCSockets : NSObject <PCSocketsDelegate> {
*/



@interface PCSockets : NSObject {

//@protected
	
//	id delegate;
	
	BOOL m_bStatus;
	
	NSString*  m_sIPAddress;
	
	CFWriteStreamRef m_writeStream;// = NULL;
	CFReadStreamRef  m_readStream;//  = NULL;
	
	CFStringRef m_host;
	UInt32		m_port;
	
	BOOL m_bConnectingAndOpenning;
	BOOL m_bConnecting;
	BOOL m_bOpenning;
	
	
	dispatch_queue_t  m_Queue; // = dispatch_queue_create("My Queue Yo!", NULL);	
	dispatch_source_t m_timerSource; // = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, myQueue);
	BOOL           m_bTimerSourceResumed; // this will prevent multiple calls to resume

	
	NSString* m_sTest; // = "test string";

	NSString* m_sSocketReceived;	
	
//	NSObject delegate;

@private
	BOOL m_bPrivate;

@public	
	BOOL m_bPublic;
	
	Boolean m_bLog; // similar variable located in NSLOG1.h used if no module level is declared
	
}


//@property(nonatomic,assign) id<PCSocketsDelegate>   delegate;


- (void)setIPAddress:(NSString*)sIPAddress;
- (void)setPort:(UInt32)port;


- (void)connectAndOpen;
- (void)socketConnect;
- (void)socketOpen;

- (BOOL)okToSend;
- (int)socketSend:(NSString*)s;
- (int)socketReceive;

- (BOOL)boolStatus;
- (void)socketClose;

- (int)status;


- (void)dispatchQueueTimerResume;
- (void)dispatchQueueTimerSuspend;


// tests
- (void)setString:(NSString*)s;
- (NSString*)getString;

- (BOOL) getPrivate;


//- (BOOL)status1;


@end








