//
//  FlipsideViewController.h
//  X10Controller
//
//  Created by Elvis on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NSLOG1.h"
#import <UIKit/UIKit.h>

@class PersistentInfo;
@class ValidEntry;  // currently not used at runtime, can be used as a reference for creating a new class

@protocol FlipsideViewControllerDelegate;


@interface FlipsideViewController : UIViewController { // <UITextFieldDelegate, UITextViewDelegate>{
	
	id <FlipsideViewControllerDelegate> delegate1;

	IBOutlet UIScrollView *scrollView;	
	
	CGRect  m_frame;
	CGPoint m_offset;
	CGSize  m_contentSize;
	
	BOOL m_bEditingTextField;	
	
	
	UITextField *m_currentTextField;	
	
	IBOutlet UIButton *m_closeKeyboard; // Referencing Outlets in Interface Builder
	IBOutlet UIButton *m_done; 
	
	IBOutlet UITextField *textIPAddress;
	IBOutlet UITextField *textPortNumber;

	IBOutlet UITextField *textHouseLetter1;
	IBOutlet UITextField *textModuleNumber1;
	IBOutlet UITextField *textModuleName1;

	IBOutlet UITextField *textHouseLetter2;
	IBOutlet UITextField *textModuleNumber2;
	IBOutlet UITextField *textModuleName2;

	IBOutlet UITextField *textHouseLetter3;
	IBOutlet UITextField *textModuleNumber3;
	IBOutlet UITextField *textModuleName3;

	IBOutlet UITextField *textHouseLetter4;
	IBOutlet UITextField *textModuleNumber4;
	IBOutlet UITextField *textModuleName4;

	IBOutlet UITextField *textHouseLetter5;
	IBOutlet UITextField *textModuleNumber5;
	IBOutlet UITextField *textModuleName5;

	IBOutlet UITextField *textHouseLetterDefault;	
		
	NSMutableArray *m_arTextFieldKeyNames;
	

//	PersistentInfo *m_persistInfo;
	

	NSSet *m_pSetHouseLetter;  // pSet - pointer to set
	NSSet *m_pSetHouseNumber;
	
	Boolean m_bLog; //  similar variable located in NSLOG1.h

	IBOutlet UISwitch *m_switchDebug;
	
	
// testing	
	NSMutableArray *m_ma;
	
	
	
}


//@property (nonatomic, retain) NSArray *m_arTextFieldKeyNames;

@property (nonatomic, assign) id <FlipsideViewControllerDelegate> delegate1;
@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UIButton *m_closeKeyboard;
@property (nonatomic, retain) UIButton *m_done;


@property (nonatomic, retain) UITextField *textIPAddress;
@property (nonatomic, retain) UITextField *textPortNumber;

@property (nonatomic, retain) UITextField *textHouseLetter1;
@property (nonatomic, retain) UITextField *textModuleNumber1;
@property (nonatomic, retain) UITextField *textModuleName1;

@property (nonatomic, retain) UITextField *textHouseLetter2;
@property (nonatomic, retain) UITextField *textModuleNumber2;
@property (nonatomic, retain) UITextField *textModuleName2;


@property (nonatomic, retain) UITextField *textHouseLetter3;
@property (nonatomic, retain) UITextField *textModuleNumber3;
@property (nonatomic, retain) UITextField *textModuleName3;

@property (nonatomic, retain) UITextField *textHouseLetter4;
@property (nonatomic, retain) UITextField *textModuleNumber4;
@property (nonatomic, retain) UITextField *textModuleName4;

@property (nonatomic, retain) UITextField *textHouseLetter5;
@property (nonatomic, retain) UITextField *textModuleNumber5;
@property (nonatomic, retain) UITextField *textModuleName5;

@property (nonatomic, retain) UITextField *textHouseLetterDefault;

@property (nonatomic, retain) PersistentInfo* m_persistInfo;

@property (nonatomic, retain) ValidEntry* m_validEntry;


@property (nonatomic, retain) NSSet *m_pSetHouseLetter;  // pSet - pointer to set
@property (nonatomic, retain) NSSet *m_pSetModuleNumber;



@property (assign) IBOutlet UISwitch		*switchDebug;


- (IBAction)done:(id)sender;

- (IBAction)btnCloseKeyboard; // Events in Interface Builder

- (IBAction)switchDebug:(id)sender;

/*
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey;

- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault;
- (NSString *)retrieveFromUserDefaults:(NSString *)sKey  defaultValue:(NSString *)sDefault saveDefault:(BOOL)bSave;

*/


@property (nonatomic, retain) NSSet *m_pSet1;
@property (nonatomic, retain) NSString *m_sBeforeEdit;


@property (nonatomic, retain) NSString *m_sCurrentTextFieldTextBeforeEdit;

//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

// testing
@property (nonatomic, retain) NSMutableArray *m_ma;
- (void)uiArray;
- (void)hideControls:(BOOL)hide;


@end // @interface





@protocol FlipsideViewControllerDelegate

- (void)flipsideViewControllerDidFinish:(FlipsideViewController *)controller;



@end // @protocol

