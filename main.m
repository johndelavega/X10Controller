//
//  main.m
//  X10Controller
//
//  Created by John on 8/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
//  Last updated 11/13/2014
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal = UIApplicationMain(argc, argv, nil, nil);
    [pool release];
    return retVal;
}



//---------------------------------------------------------------
static NSString * const m_sVersion = @"0.1.014"; // m.n.bbb = build bbb
//---------------------------------------------------------------
NSString * _egd_appVersion()
{
	return m_sVersion;
}