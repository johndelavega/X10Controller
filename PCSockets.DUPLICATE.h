//
//  PCSockets.h - PC = Persistent Connection
//  SocketApp
//
//  Created by Elvis on 8/16/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>




/*
@protocol PCSockets <NSObject>

- (id) delegate;

@end
*/


/*
@protocol PCSocketsDelegate <NSObject>

@optional

- (void) infoOnlyProtocolMethod;

@end

@interface PCSockets : NSObject <PCSocketsDelegate> {
*/



@interface PCSockets : NSObject {

//@protected
	
//	id delegate;
	
	Boolean m_bStatus;
	
	NSString *m_sIPAddress;
	
	CFWriteStreamRef m_writeStream;//  = NULL;
	CFReadStreamRef  m_readStream; //  = NULL;
	
	CFStringRef m_host;
	UInt32		m_port;
	
	Boolean m_bConnectingAndOpenning;
	Boolean m_bConnecting;
	Boolean m_bOpenning;
	
	
	dispatch_queue_t  m_Queue; // = dispatch_queue_create("My Queue Yo!", NULL);	
	dispatch_source_t m_timerSource; // = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, myQueue);
	

	NSString *m_sTest; // = "test string";
	
//	NSObject delegate;

@private
	Boolean m_bPrivate;

@public	
	Boolean m_bPublic;
	
}


//@property(nonatomic,assign) id<PCSocketsDelegate>   delegate;


- (void)setIPAddress:(NSString *)sIPAddress;
- (void)setPort:(UInt32)port;

- (Boolean)socketReceive2;
- (void)connectAndOpen;
- (void)socketConnect;
- (void)socketOpen;

- (Boolean)okToSend;
- (int)socketSend:(NSString *)s;
- (int)socketReceive1;

   

- (Boolean)boolStatus;
- (void)socketClose;

- (int)status;


// tests
- (void)setString:(NSString *)s;
- (NSString *)getString;

- (Boolean) getPrivate;


//- (Boolean)status1;


@end








